import React from "react";
import "./App.css";
import NumberButton from "./components/ButtonComponents/NumberButton";
import ActionButton from "./components/ButtonComponents/ActionButton";
import CalculatorDisplay from "./components/DisplayComponents/CalculatorDisplay";

class App extends React.Component {
  constructor() {
    super();
    this.row1 = [9, 8, 7];
    this.state = {
      result: 0,
      expression: "",
      lastaction: 0
    };
    this.arr = [9, 8, 7, 6, 5, 4, 3, 2, 1, 0];
  }

  operation(input, action) {
    console.log(input, "clicked", action);
    if (input == "c") {
      this.setState({
        expression: ""
      });
    } else {
      if (this.state.expression == "") {
        if (input >= 0 && input <= 9) {
          let expres = input.toString();
          this.setState({
            expression: expres
          });
        }
      } else {
        if (input == 61) {
          let ans = eval(this.state.expression);
          this.setState({
            expression: ans,
            lastaction: 0
          });
        } else {
          if (this.state.lastaction == 0) {
            if (input >= 0 && input <= 9) {
              let expres = this.state.expression + input.toString();
              this.setState({
                expression: expres,
                lastaction: action
              });
            } else {
              let expres = this.state.expression + String.fromCharCode(input);
              console.log(eval(this.state.expression));
              this.setState({
                expression: expres,
                lastaction: action
              });
            }
          } else {
            if (input >= 0 && input <= 9) {
              let expres = this.state.expression + input.toString();
              this.setState({
                expression: expres,
                lastaction: action
              });
            }
          }
        }
      }
    }
  }

  render() {
    let x = this.row1.map(value => {
      // if (value >= 7) {

      // console.log(value);

      return (
        <NumberButton
          data={value}
          action={0}
          input={value}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        >
          {this.props.input}
        </NumberButton>
      );
      // }
    });

    console.log(x);

    return (
      <div className="calculator">
        <CalculatorDisplay
          data={this.state.expression == "" ? 0 : this.state.expression}
          class="display"
        />
        <NumberButton
          data="Clear"
          class="numbutton-extra"
          action={0}
          input={"c"}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        />
        <ActionButton
          data="/"
          action={1}
          input={47}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        />

        {/* <NumberButton
          data="9"
          onclick={() => {
            this.operation(9, 0);
          }}
          action={0}
          input={9}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        />
        <NumberButton
          data="8"
          action={0}
          input={8}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        />
        <NumberButton
          data="7"
          action={0}
          input={7}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        /> */}
        {x}

        <ActionButton
          data="*"
          action={1}
          input={42}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        />
        <NumberButton
          data="6"
          action={0}
          input={6}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        />
        <NumberButton
          data="5"
          action={0}
          input={5}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        />
        <NumberButton
          data="4"
          action={0}
          input={4}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        />
        <ActionButton
          data="-"
          action={1}
          input={45}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        />
        <NumberButton
          data="3"
          action={0}
          action={0}
          input={3}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        />
        <NumberButton
          data="2"
          action={0}
          input={2}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        />
        <NumberButton
          data="1"
          action={0}
          input={1}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        />
        <ActionButton
          data="+"
          action={1}
          input={43}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        />
        <NumberButton
          data="0"
          class="numbutton numbutton-extra"
          action={0}
          input={0}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        />
        <ActionButton
          data="="
          action={1}
          input={61}
          onclick={(i, j) => {
            this.operation(i, j);
          }}
        />
      </div>
    );
  }
}

export default App;
