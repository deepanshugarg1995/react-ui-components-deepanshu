import React from "react";
import "./Display.css";
function CalculatorDisplay(props) {
  return (
    <div className={`display-div ${props.class}`}>
      {" "}
      <span> {props.data} </span>
    </div>
  );
}
export default CalculatorDisplay;
