import React from "react";
import "./Button.css";
function NumberButton(props) {
  return (
    <div
      className={`numberbutton  ${props.class} `}
      onClick={() => {
        props.onclick(props.input, props.action);
      }}
    >
      {props.data}
    </div>
  );
}
export default NumberButton;
