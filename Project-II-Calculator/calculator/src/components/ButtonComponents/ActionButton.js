import React from "react";
import "./Button.css";

function ActionButton(props) {
  return (
    <div
      className="actionbutton"
      onClick={() => {
        console.log("action button");
        props.onclick(props.input, props.action);
      }}
    >
      {props.data}
    </div>
  );
}
export default ActionButton;
