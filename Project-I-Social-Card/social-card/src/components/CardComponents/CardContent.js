import React from "react";
import "./Card.css";
function CardContent() {
  return (
    <React.Fragment>
      <article>
        <div className="article-title">Get started with React</div>
        <div className="article-content">
          React make it painless to create interactive UI's. Design simple view
          for each state in your application
        </div>
        <div className="article-footer">reactjs.org</div>
      </article>
    </React.Fragment>
  );
}
export default CardContent;
