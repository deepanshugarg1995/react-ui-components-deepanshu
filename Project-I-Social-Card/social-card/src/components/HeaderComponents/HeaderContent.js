import React from "react";
import "./Header.css";
import HeaderTitle from "./HeaderTitle";
function HeaderContent() {
  return (
    <div className="header headercontent">
      <HeaderTitle />

      <div className="headercontent-div">
        Lets learn React by building simple interface with component. DOnt Try
        to overthink it, just keep it simple and have fun. ONce you feel
        comfortable using components you are well on your way to mastering it!
      </div>
    </div>
  );
}
export default HeaderContent;
