import React from "react";
import "./Header.css";
function HeaderTitle() {
  let d = new Date();
  let date = d.toDateString().split(" ");
  return (
    <div className="headertitle">
      <div className="headertitle-flex">
        <div className="headertitle-span headertitle-span-inline ">
          Lambda School
        </div>
        <div className="headertitle-span-inline headertitle-span-at ">
          &nbsp; &nbsp; @LambdaSchool
          {date[1]} {date[2]}
        </div>
      </div>
    </div>
  );
}
export default HeaderTitle;
