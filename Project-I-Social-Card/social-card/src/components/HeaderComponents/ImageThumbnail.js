import React from "react";
import "./Header.css";
function ImageThumbnail() {
  return (
    // <div className="headerimage ">
    <React.Fragment>
      {/* <img
        className="headerimage-img"
        src="https://tk-assets.lambdaschool.com/1c1b7262-cf23-4a9f-90b6-da0d3c74a5c6_lambdacrest.png"
      /> */}
      <img src={require("./lambdacrest.png")} className="headerimage-img" />
      {/* // </div> */}
    </React.Fragment>
  );
}
export default ImageThumbnail;
